from bs4 import BeautifulSoup
from PIL import Image
import subprocess
import os
import json

INTERPOLATIONS = [
        'auto', # == TCB
        'clamped',
        'constant',
        'linear',
        'halt', # == ease
        ]

SCANNING_ROW = 10

PNG_NAME = 'temp.%04d.png'

def amount_for_frame(n):
    filename = PNG_NAME % (n,)
    image = Image.open(filename)
    os.unlink(filename)

    w, h = image.size

    pixels = image.getdata()

    start = w*SCANNING_ROW

    for i in range(start, start+w):
        if pixels[i][1]==0:
            return i-start


    return w

def main():

    results = {}

    with open('test.sif', 'r') as f:
        sif = BeautifulSoup(f, features='xml')

    waypoints = sif('waypoint')

    for first in INTERPOLATIONS:
        waypoints[0]['after'] = first

        for last in INTERPOLATIONS:
            waypoints[1]['before'] = last

            key = f'{first}-{last}'
            results[key] = []

            with open('temp.sif', 'w') as f:
                f.write(str(sif))

            subprocess.call(
                    ['synfig', 'temp.sif']
                    )

            for frame in range(100):
                results[key].append(
                        amount_for_frame(frame))

    with open('interpolation-tests.json', 'w') as f:
        json.dump(results, f, indent=2, sort_keys=True)

if __name__=='__main__':
    main()
