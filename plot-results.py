import json
import sys

INTERPOLATIONS = [
        'auto', # == TCB
        'clamped',
        'constant',
        'linear',
        'halt', # == ease
        ]

data = json.load(open('interpolation-tests.json', 'r'))

sys.stdout.write(f"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   width="{20+600*len(INTERPOLATIONS)}"
   height="{20+110*len(INTERPOLATIONS)}"
   viewBox="0 0 1016 571.50002"
   version="1.1"
   id="svg8"
   inkscape:version="1.2.2 (b0a8486541, 2022-12-01)"
   sodipodi:docname="temp.svg"
   xml:space="preserve"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:dc="http://purl.org/dc/elements/1.1/">
   """)

x = -950

for left in INTERPOLATIONS:

    y = 10

    for right in INTERPOLATIONS:

        command = 'M'
        path = []
        for i, value in enumerate(data[f'{left}-{right}']):
            path.append(command)
            path.append(f'{i*3},{value/10}')

            command = 'L'

        path = ' '.join(path)

        sys.stdout.write(f"""
        <g transform="translate({x},{y})">
   <image
       width="100"
       height="100"
       preserveAspectRatio="none"
       style="image-rendering:optimizeQuality"
       xlink:href="{left}.png"
       x="0"
       y="0" />

    <rect
       style="stroke-width:1px;stroke:#DDD;fill:#CCC;"
       width="300"
       height="100"
       x="110"
       y="0" />

   <image
       width="100"
       height="100"
       preserveAspectRatio="none"
       style="image-rendering:optimizeQuality"
       xlink:href="{right}.png"
       x="410"
       y="0" />

    <path
        transform="translate(110, 0)"
       style="fill:none;fill-rule:evenodd;stroke:black;stroke-width:5px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
        d="{path}"
        />

       </g>
       """)

        y += 110

    x += 600

sys.stdout.write("""
</svg>""")
